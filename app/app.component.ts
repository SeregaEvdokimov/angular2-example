/**
 * Created by s.evdokimov on 22.12.2016.
 */

import {Component} from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
    <h1>My First Angular 2 App</h1>
    <ul>
      <li><a routerLink="/">Home</a></li>
      <li><a routerLink="/about">About</a></li>
    </ul>
    <hr>
    <router-outlet></router-outlet>
  `,
})

export class AppComponent {}
